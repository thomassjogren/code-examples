import { shallow } from '@vue/test-utils';
import CustomInput from '@/components/CustomInput.vue';

describe('CustomInput.vue', () => {
  it('accepts prop type for input', () => {
    const wrapper = shallow(CustomInput, {
      propsData: { type: 'number' },
    });

    expect(wrapper.props().type).toBe('number');
    expect(wrapper.find('input').attributes().type).toBe('number');
  });

  it('renders formAddon slot', () => {
    const wrapper = shallow(CustomInput, {
      slots: { formAddon: '<span>Test</span>' },
    });

    expect(wrapper.find('span').exists()).toBeTruthy();
    expect(wrapper.find('span').text()).toBe('Test');
  });

  it('set wrapper focus when input gets focus', () => {
    const wrapper = shallow(CustomInput);
    const wrapperElement = wrapper.find('.input-group');

    const input = wrapper.find('input');
    input.trigger('focus');
    expect(wrapperElement.classes()).toContain('focus');

    input.trigger('blur');
    expect(wrapperElement.classes()).not.toContain('focus');
  });

  it('accepts value and emit input change', () => {
    const wrapper = shallow(CustomInput, {
      propsData: {
        value: '123',
      },
    });

    const input = wrapper.find('input');

    expect(input.element.value).toEqual('123');

    input.element.value = 'abc';
    input.trigger('change');

    expect(wrapper.emitted().input).toBeTruthy();
    expect(wrapper.emitted().input[0]).toEqual(['abc']);
  });
});
