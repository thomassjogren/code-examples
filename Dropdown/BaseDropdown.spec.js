import { shallowMount, createLocalVue } from '@vue/test-utils';
import BaseDropdown from '@/components/UI/Base/BaseDropdown';

const localVue = createLocalVue();

describe('BaseDropdown.vue', () => {
  it('mounts correctly', () => {
    const wrapper = shallowMount(BaseDropdown);
    expect(wrapper.isVueInstance()).toBeTruthy();
  });

  it('correctly sets selected item', () => {
    const wrapper = shallowMount(BaseDropdown, {
      propsData: {
        items: [
          { id: 1 },
          { id: 2 },
        ],
      },
    });
    expect(wrapper.vm.selectedItem).toStrictEqual({ id: '' });

    wrapper.setProps({ selectedId: 1 });
    expect(wrapper.vm.selectedItem).toStrictEqual({ id: 1 });

    wrapper.setProps({ selectedId: '' });
    expect(wrapper.vm.selectedItem).toStrictEqual({ id: '' });
  });

  it('emit \'update\' when item is selected', () => {
    const wrapper = shallowMount(BaseDropdown, {
      propsData: {
        items: [
          { id: 1 },
          { id: 2 },
        ],
      },
    });
    wrapper.vm.setSelectedId(1);
    expect(wrapper.emitted('update')).toBeTruthy();
  });

  it('hides when show is set to false', async () => {
    const wrapper = shallowMount(BaseDropdown);
    wrapper.setData({ show: true });
    await localVue.nextTick();
    expect(wrapper.find('.base-dropdown__list').isVisible()).toBeTruthy();

    wrapper.vm.hide();
    await localVue.nextTick();
    expect(wrapper.find('.base-dropdown__list').isVisible()).toBeFalsy();
  });
});
