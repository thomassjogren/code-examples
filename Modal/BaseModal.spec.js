import { shallowMount, createLocalVue } from '@vue/test-utils';
import BaseModal from '@/components/UI/Base/BaseModal';

const localVue = createLocalVue();

describe('BaseModal.vue', () => {
  it('mounts correctly', () => {
    const wrapper = shallowMount(BaseModal, {
      propsData: {
        open: true,
      },
    });

    expect(wrapper.find('.base-modal__container').attributes('style')).toBe(
      'max-width: 800px; max-height: 75%; border-radius: 0; padding: 15px; overflow: auto;',
    );
  });

  it('emits \'hide\' event when closed', () => {
    const wrapper = shallowMount(BaseModal, {
      propsData: {
        open: true,
        closeButton: true,
      },
    });

    wrapper.find('.close-button').trigger('click');
    expect(wrapper.emitted('hide')).toBeTruthy();
  });

  it('opens and closes when \'open\' changes', async () => {
    const wrapper = shallowMount(BaseModal);
    expect(wrapper.find('.base-modal__container').exists()).toBeFalsy();

    const spy = jest.spyOn(wrapper.vm, 'toggleModal');

    wrapper.setProps({ open: true });
    await localVue.nextTick();
    expect(spy).toHaveBeenLastCalledWith(true);
    expect(wrapper.find('.base-modal__container').exists()).toBeTruthy();

    wrapper.setProps({ open: false });
    await localVue.nextTick();
    expect(spy).toHaveBeenLastCalledWith(false);
  });

  it('closes if click-outside is set and a click occur', async () => {
    const wrapper = shallowMount(BaseModal,
      {
        propsData: {
          clickOutside: true,
        },
      });

    const spy = jest.spyOn(wrapper.vm, 'close');
    const overlay = wrapper.find('.base-modal__overlay');

    overlay.trigger('click');
    expect(spy).toHaveBeenCalledTimes(1);

    wrapper.setProps({
      clickOutside: false,
    });
    await localVue.nextTick();
    overlay.trigger('click');
    expect(spy).toHaveBeenCalledTimes(1);
  });
});
