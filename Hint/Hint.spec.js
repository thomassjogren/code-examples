import { shallow } from '@vue/test-utils';
import Hint from '@/components/Hint.vue';

describe('Hint.vue', () => {
  it('renders with default props', () => {
    const wrapper = shallow(Hint);

    expect(wrapper.classes()).toEqual(['hint']);
  });

  it('accepts states and sets classes', () => {
    const wrapper = shallow(Hint, {
      propsData: {
        info: true,
      },
    });

    expect(wrapper.classes()).toContain('info');
  });

  it('renders the default slot', () => {
    const text = '<span>Text for hint</span>';

    const wrapper = shallow(Hint, {
      slots: {
        default: text,
      },
    });

    expect(wrapper.html()).toContain(text);
  });
});
