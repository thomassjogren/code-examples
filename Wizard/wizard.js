import Vue from 'vue'
import filter from 'lodash/filter'
import find from 'lodash/find'
import forEach from 'lodash/forEach'

export default {
  namespaced: true,

  state: {
    steps: [],

    initialStep: 0,
    currentStep: 0,
    isComplete: false
  },

  mutations: {
    ADD_STEP: (state, step) => {
      state.steps.push(step)
    },

    SET_VALID_STEP: (state, step) => {
      Vue.set(find(state.steps, step), 'valid', true)
    },

    SET_INVALID_STEP: (state, step) => {
      Vue.set(find(state.steps, step), 'valid', false)
    },

    CLEAR_TO_STEP: (state, steps) => {
      forEach(steps, (step) => {
        const stepIndex = state.steps.indexOf(step)

        state.steps[stepIndex].valid = false
        state.steps[stepIndex].checked = false
        state.steps[state.currentStep].checked = true
      })
    },

    NAVIGATE_TO_STEP: (state, { oldStep, newStep }) => {
      state.steps[oldStep].active = false
      state.steps[newStep].active = true
      state.steps[newStep].checked = true

      state.currentStep = newStep
      state.isComplete = false
    },

    CLEAR: (state) => {
      state.steps = []
      state.currentStep = 0
      state.isComplete = false
    },

    COMPLETE: (state) => {
      state.isComplete = true
    },

    INITIALIZE: (state) => {
      state.steps[state.initialStep].active = true
      state.steps[state.initialStep].checked = true
      state.isComplete = false
    }
  },

  actions: {
    addStep: ({ commit }, step) => {
      commit('ADD_STEP', step)
    },

    nextStep: ({ commit, state }) => {
      commit('SET_VALID_STEP', state.steps[state.currentStep])

      if ((state.currentStep + 1) >= state.steps.length) {
        commit('COMPLETE')
        return
      }

      commit('NAVIGATE_TO_STEP', { oldStep: state.currentStep, newStep: state.currentStep + 1 })
    },

    navigateTo: ({ commit, state }, newStep) => {
      const newIndex = state.steps.indexOf(newStep)

      commit('NAVIGATE_TO_STEP', { oldStep: state.currentStep, newStep: newIndex })
    },

    clear: ({ commit }) => {
      commit('CLEAR')
    },

    clearToStep: ({ commit, state }, stepIndex) => {
      const stepsToReset = filter(state.steps, (step) => {
        return step !== state.steps[stepIndex]
      })
      commit('CLEAR_TO_STEP', stepsToReset)
    },

    initialize: ({ commit }) => {
      commit('INITIALIZE')
    }
  },

  getters: {
    steps: (state) => {
      return state.steps
    },

    currentStep: (state) => {
      return state.currentStep
    }
  }
}