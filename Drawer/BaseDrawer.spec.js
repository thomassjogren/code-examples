import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import BaseDrawer from '@/components/UI/Base/BaseDrawer.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('BaseDrawer.vue', () => {
  test('should be named "BaseDrawer"', () => {
    const wrapper = shallowMount(BaseDrawer);
    expect(wrapper.name()).toBe('BaseDrawer');
  });

  test('should emit "hide" on close', () => {
    const wrapper = shallowMount(BaseDrawer, {
      propsData: {
        open: true,
      },
    });
    wrapper.find('.base-drawer__overlay').trigger('click');
    expect(wrapper.emitted().hide).toBeTruthy();
  });

  test('matches snapshot', () => {
    const wrapper = shallowMount(BaseDrawer, {
      propsData: {
        open: true,
        maxWidth: '300px',
      },
    });
    expect(wrapper.element).toMatchSnapshot();
  });
});
